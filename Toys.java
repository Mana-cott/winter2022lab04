public class Toys{
	
	//fields
	private String theme;
	//theme of the toy line
	//ex. robots, pirates, castles
	private String material;
	//name of the material used
	//ex. plastic, fabric, foam, slime
	private int ageLevel;
	//recommended age level
	
	
	//constuctor method
	public Toys(String theme, String material, int ageLevel){
		this.theme = theme;
		this.material = material;
		if(ageLevel > 0 && ageLevel <= 99){
			this.ageLevel = ageLevel;
		}
	}
	
	//getter methods
	public String getTheme(){
		return this.theme;
	}
	public String getMaterial(){
		return this.material;
	}
	public int getAgeLevel(){
		return this.ageLevel;
	}
	
	//setter methods
	
	public void setTheme(String theme){
		this.theme = theme;
	}
	public void setMaterial(String material){
		this.material = material;
	}
	public void setAgeLevel(int ageLevel){
		this.ageLevel = ageLevel;
	}
	
	
	//instance method
	public void toyAction(){
		if(this.theme.equals("robots")){
			System.out.println("BEEP");
		}
		else if(this.theme.equals("pirates")){
			System.out.println("AARGH!");
		}
		else if(this.theme.equals("castles")){
			System.out.println("AAAAA! for the castle!");
		}
		else
		{
			System.out.println("KABLAM!");
		}
	}
}
	
