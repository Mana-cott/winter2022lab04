import java.util.Scanner;
public class Shop{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		Toys[] toy = new Toys[4];
		for(int i = 0; i < toy.length; i++){
			
			//fancy stuff to make it more interesting for user
			String ordinal = " ";
			if(i == 0){
				ordinal = "first";
			}
			else if(i == 1){
				ordinal = "second";
			}
			else if(i == 2){
				ordinal = "third";
			}
			else if(i == 3){
				ordinal = "fourth";
			}
			
			//Using Constructor
			int questionNumber = i + 1;
			System.out.println("~~ " + questionNumber + ".");
			System.out.println("Please insert the theme for your " + ordinal + " toy!");
			System.out.println("Example: robots, pirates, castles, etc...");
			String setTheme = scan.next();
			System.out.println("Please insert the recommended age level for your " + ordinal + " toy.");
			int setAgeLevel = scan.nextInt();
			System.out.println("What material is the " + ordinal + " toy made from?");
			System.out.println("Example: plastic, fabric, foam, slime, etc...");
			String setMaterial = scan.next();
			
			toy[i] = new Toys(setTheme, setMaterial, setAgeLevel);
			System.out.println(ordinal + " toy is: ");
			System.out.println(toy[i].getTheme() + ", " + toy[i].getAgeLevel() + ", " + toy[i].getMaterial());
			System.out.println(" ");
		}
		
		//printing back last fields
		System.out.println("Now the last three fields entered will be printed back.");
		System.out.println(toy[3].getTheme());
		System.out.println(toy[3].getAgeLevel());
		System.out.println(toy[3].getMaterial());
		System.out.println(" ");
		
		//Using Set-Methods
		System.out.println("Do you wish to re-enter your fields for the last set of toys?");
		System.out.println("TYPE: 1 = yes, 2 = no");
		int answer = scan.nextInt();
		if(answer == 1){
			System.out.println("Please re-enter fourth toy theme:");
			toy[3].setTheme(scan.next());
			System.out.println("Please re-enter fourth toy age level:");
			toy[3].setAgeLevel(scan.nextInt());
			System.out.println("Please re-enter fourth toy material:");
			toy[3].setMaterial(scan.next());
		}
		
		//Using Get-Methods
		//printing back last fields again
		System.out.println("Now the last three fields entered will be printed back.");
		System.out.println(toy[3].getTheme());
		System.out.println(toy[3].getAgeLevel());
		System.out.println(toy[3].getMaterial());
		//sound effect action
		toy[3].toyAction();
		
	}	
}
